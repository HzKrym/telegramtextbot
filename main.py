import telebot as tb
from settings import *

bot = tb.TeleBot(TOKEN)
tb.apihelper.proxy = {'https': 'socks5://' + PROXY_LOGIN + ':' + PROXY_PASSWORD + '@' + PROXY_IP + ':' + PROXY_PORT}
chat_ids = []
f = open("ids")
for line in f:
    chat_ids.append(int(line))
f.close()


def user_to_str(user):
    return str(user.first_name) + " " + str(user.last_name) + "(" + str(user.username) + ')'


@bot.message_handler(commands=["start"])
def start_bot(m):
    f = open("ids")
    for line in f:
        if int(line) == m.chat.id:
            return -1
    f = open('ids', 'at')
    f.write(str(m.chat.id) + '\n')
    f.close()
    chat_ids.append(m.chat.id)
    print('Welcome ' + user_to_str(m.from_user) + "!")
    return 1


@bot.message_handler(content_types=["text"])
def echo_all(m):
    for id in chat_ids:
        if id != m.chat.id:
            bot.send_message(id, m.text)
    print(user_to_str(m.from_user) + ': ' + m.text)


for id in chat_ids:
    bot.send_message(id, "Сервер робит!")
bot.polling()
for id in chat_ids:
    bot.send_message(id, "Сервер не робит! :с")